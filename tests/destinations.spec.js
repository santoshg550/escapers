const chai = require('chai')
const chaiHttp = require('chai-http')

const { prep, cleanDB } = require('./prep')
const validations = require('../routes/validations')

const expect = chai.expect

chai.use(chaiHttp)

describe('destinations', function() {
    let server

    before((done) => {
        prep((svr) => {
            server = svr
            done()
        })
    })

    describe('destination', function(done) {
        it('abc is right', function(done) {
            expect(validations.dest.isDestValid('abc')).to.be.equal(true)

            done()
        })

        let wrong = [ '', 4 ]

        wrong.forEach(function(dest) {
            it(dest + ' is wrong', function(done) {
                expect(validations.dest.isDestValid(dest)).to.be.equal(false)

                done()
            })
        })
    })

    it('it should load destination page', function(done) {
        chai
            .request(server)
            .get('/destinations/manali')
            .end(function(err, res) {
                expect(err).to.be.null

                expect(res).to.have.status(200)

                done()
            })
    })
})
