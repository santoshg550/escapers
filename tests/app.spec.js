const EventEmitter = require('events')

const chai = require('chai')
const chaiHttp = require('chai-http')

const { prep, cleanDB } = require('./prep')
const updateApp = require('../update_app')

const expect = chai.expect

chai.use(chaiHttp)

describe('app object', function() {
    let server

    before((done) => {
        prep((svr) => {
            server = svr
            done()
        })
    })

    describe('update app', () => {
        it('update app when connections are available', (done) => {
            const app = new EventEmitter

            updateApp(app)

            app.on('updated', (err) => {
                expect(err).to.be.undefined

                expect(app).to.have.property('agency')
                expect(app).to.have.property('destinations')
                expect(app).to.have.property('home')

                done()
            })
        })
    })
})
