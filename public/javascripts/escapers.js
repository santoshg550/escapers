$(document).ready(() => {
    $('.carousel-inner div:first-child').addClass('active')

    setInterval(function() {
        const slider = document.querySelector('.slider')
        const images = document.querySelectorAll('.slider .img-slide')

        if(images[0].clientWidth/slider.clientWidth == 1) {
            images[0].style.left = '-100%'
            images[1].style.left = '0'
            images[2].style.left = '100%'
            images[3].style.left = '200%'
        } else if(images[0].clientWidth/slider.clientWidth == 0.5) {
            images[0].style.left = '-50%'
            images[1].style.left = '0'
            images[2].style.left = '50%'
            images[3].style.left = '100%'
        } else {
            images[0].style.left = '-33.33%'
            images[1].style.left = '0'
            images[2].style.left = '33.33%'
            images[3].style.left = '66.66%'
        }

        setTimeout(function() {
            if(images[0].clientWidth/slider.clientWidth == 1)
                images[0].style.left = '300%'
            else if(images[0].clientWidth/slider.clientWidth == 0.5)
                images[0].style.left = '150%'
            else
                images[0].style.left = '100%'

            images[0].remove()
            $('.slider').append(images[0])
        }, 2000)
    }, 4000)
})
