$(document).ready(() => {
    $('.navbar-nav > li:last-child').click(() => {
        $('#mySidenav').css('width', '250px')
    })

    $('#mySidenav a:first-child').click(() => {
        $('#mySidenav').css('width', '0')
    })

    $('.navbar-nav li').click(function() {
        $(this).addClass('active')
        $(this).siblings('.active').removeClass('active')
    })

    /*
     * For some unknown reason, contacts at the top are displayed differently in
     * browsers responsive view and on the actual mobile device. So, setting
     * body padding with CSS may not produce desired results.
     */
    $(window).resize(padBody)

    function padBody() {
        const navbar = $('.navbar')
        const sticky = navbar.height()

        $('body').css('padding-top', sticky)
    }

    padBody()
})
