$(document).ready(function() {
    $('.destMeta div').click(function() {
        $('.mid-1 .active').removeClass('active')

        $('.mid-1 ' + $(this).attr('data-toggle')).addClass('active')

        $(this).siblings('.selMeta').removeClass('selMeta')
        $(this).addClass('selMeta')
    })

    $('.navbar-nav .active').removeClass('active')

    $('.navbar-nav .dropdown').addClass('active')
})
