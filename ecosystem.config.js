module.exports = {
    apps: [
        name: 'escapers',
        script: './server.js',
        watch: true,
        env: {
            PORT: 3000,
            PLATFORM: 'IaaS',
            DATABASE_URI: 'mongodb://santosh:escapers@cluster0-shard-00-00-7zwaf.mongodb.net:27017,cluster0-shard-00-01-7zwaf.mongodb.net:27017,cluster0-shard-00-02-7zwaf.mongodb.net:27017/escapers?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true'
        }
    ]
}
