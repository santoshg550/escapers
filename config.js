[
    'PORT',
    'PLATFORM',
    'DATABASE_URI'
].forEach((name) => {
    if(!process.env[name])
        throw new Error(`Environment variable ${name} is missing`)
})

module.exports = {
    databaseURI: process.env.DATABASE_URI,
    port: process.env.PORT
}
