const Agency = require('./models/agency')
const Destination = require('./models/destination')
const Home = require('./models/home')

module.exports = (app) => {
    let update = 3

    Agency.findOne((err, agency) => {
        if(err)
            return app.emit('updated', err)

        if(!agency)
            return app.emit('updated', new Error('Agency info is absent in database'))

        app.agency = agency

        if(--update == 0)
            app.emit('updated')
    })

    Destination.find((err, destinations) => {
        if(err)
            return app.emit('updated', err)

        if(!destinations)
            return app.emit('updated', new Error('Destinations info is absent in database'))

        app.destinations = destinations

        if(--update == 0)
            app.emit('updated')
    })

    Home.findOne((err, home) => {
        if(err)
            return app.emit('updated', err)

        if(!home)
            return app.emit('updated', new Error('Home info is absent in database'))

        app.home = home

        if(--update == 0)
            app.emit('updated')
    })
}
