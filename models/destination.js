const mongoose = require('mongoose')

const Schema = mongoose.Schema

const destination = new Schema({
    destination: String,
    name: String,
    description: String,
    pricePerPerson: {
        triple: Number,
        twin: Number
    },
    period: Number,
    topImg: String,
    gallery: [String]
})

module.exports = mongoose.model('Destination', destination)
