const mongoose = require('mongoose')

const Schema = mongoose.Schema

const home = new Schema({
    name: String,
    carousel: [
        {
            name: String,
            destination: String,
            img: String,
            catchPhrase: String,
        }
    ],
    slider: [
        {
            img: String,
            destination: String,
        }
    ],
    tiles: [
        {
            name: String,
            destination: String,
            img: String,
            experience: String,
        }
    ],
    updatedOn: Date
})

home.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = mongoose.model('Home', home)
