const mongoose = require('mongoose')

const Schema = mongoose.Schema

const agency = new Schema({
    name: String,
    description: String,
    aboutUs: String,
    address: {
        line: String,
        country: String,
        state: String,
        city: String,
        pincode: Number
    },
    established: Date,
    logo: String,
    whatsapp: [Number],
    defMessage: String,
    mobNos: [Number],
    updatedOn: Date
})

agency.pre('save', function(next) {
    this.updatedOn = new Date

    next()
})

module.exports = mongoose.model('Agency', agency)
