const express = require('express')
const bodyParser = require('body-parser')
const compression = require('compression')
const helmet = require('helmet')
const responseTime = require('response-time')
const addRequestId = require('express-request-id')
const favicon = require('serve-favicon')
const path = require('path')

const log = require('./lib/log')
const config = require('./config')

const index = require('./routes/index')
const destinations = require('./routes/destinations')

const app = express()

app
    .use(favicon(path.join(__dirname, 'public/images', 'favicon.ico')))
    .set('view engine', 'ejs')
    .use(compression())
    .use(express.static('public'))
    .use(bodyParser.json())
    .use(helmet())
    .use(helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: ["'self'"],
            scriptSrc: [
                "'self'",
                "'unsafe-inline'",
                'ajax.googleapis.com',
                'maxcdn.bootstrapcdn.com'
            ],
            styleSrc: [
                "'self'",
                "'unsafe-inline'",
                'ajax.googleapis.com',
                'maxcdn.bootstrapcdn.com',
                'cdnjs.cloudflare.com'
            ],
            imgSrc: [
                "'self'",
                'placehold.it',
                's3.ap-south-1.amazonaws.com'
            ],
            fontSrc: ['cdnjs.cloudflare.com', 'maxcdn.bootstrapcdn.com'],
            connectSrc: ["'self'"]
        },
        setAllHeaders: true
    }))
    .use(helmet.noCache())
    .use(errors.error)
    .disable('x-powered-by')

app
    .use(addRequestId())
    .use(responseTime())
    .use((req, res, next) => {
        log.info({ req: req })

        res.header('X-Request-Id', req.id)

        function afterRes() {
            res.removeListener('finish', afterRes)
            res.removeListener('close', afterRes)

            log.info({ res: res })
        }

        res.on('finish', afterRes)
        res.on('close', afterRes)

        next()
    })
    .use('/destinations', destinations)
    .use('/', index)
    .use((req, res, next) => {
        var err = new Error('Not Found')
        err.status = 404
        next(err)
    })
    .use((err, req, res, next) => {
        log.error({ err: err }, 'Error handling middleware')

        res.status(err.status || 500).render('error', {
            message: err.message
        })
    })

module.exports = app
