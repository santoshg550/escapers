const val = require('./validations').dest
const Destination = require('../models/destination')

function validateDest(dest) {
    if(!val.isDestValid(dest))
        return errors.DestError
}

function display(req, res, next) {
    const destinations = req.app.destinations
    const agency = req.app.agency

    const dests = []
    for(let i = 0; i < destinations.length; i++)
        dests.push({
            destination: destinations[i].destination,
            name: destinations[i].name
        })

    for(let i = 0; i < destinations.length; i++)
        if(destinations[i].destination == req.params.dest)
            return res.render('destinations', {
                name: agency.name,
                mobNos: agency.mobNos,
                whatsapp: agency.whatsapp,
                defMessage: agency.defMessage,
                aboutUs: agency.aboutUs,

                destinations: dests,

                destName: destinations[i].name,
                destination: destinations[i].destination,
                topImg: destinations[i].topImg,
                description: destinations[i].description,
                pricePerPerson: destinations[i].pricePerPerson,
                period: destinations[i].period,
                gallery: destinations[i].gallery
            })

    next()
}

module.exports = {
    validateDest: validateDest,
    display: display
}
