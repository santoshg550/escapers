const { checkString } = require('../lib/validation.js')

module.exports = {
    dest: {
        isDestValid: checkString
    }
}
