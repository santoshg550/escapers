const express = require('express')

const { validateDest, display }= require('./dest_handler')

const router = express.Router()

router.use('/:dest', (req, res, next) => {
    const error = validateDest(req.params.dest)

    if(error)
        return res.status(400).end(error)

    next()
})

router.get('/:dest', display)

module.exports = router
