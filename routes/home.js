function home(req, res, next) {
    const home = req.app.home
    const agency = req.app.agency

    const dests = []
    const destinations = req.app.destinations
    for(let i = 0; i < destinations.length; i++)
        dests.push({
            destination: destinations[i].destination,
            name: destinations[i].name
        })

    res.render('welcome', {
        destinations: dests,

        name: home.name,
        carousel: home.carousel,
        slider: home.slider,
        tiles: home.tiles,

        mobNos: agency.mobNos,
        whatsapp: agency.whatsapp,
        defMessage: agency.defMessage,
        aboutUs: agency.aboutUs
    })
}

module.exports = {
    home: home
}
