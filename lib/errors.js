const checkValue = require('./validation').checkValue;
const log = require('./log');

const sww = 'Something went wrong'

function dbError(err) {
    log.error({ err: err }, 'Database error');

    return {
        code: err.code,
        message: sww
    }
}

/*
 * Method to use for synchronous errors.
 */
function buildError(err) {
    if(checkValue(err, 'String') || !err)
        return {
            error: {
                message: err || sww
            }
        }

    return { error: err }
}

function error(req, res, next) {
    res.error = function(status, err) {
        if(err && !err.error)
            err = buildError(err)

        res.status(status).json(err)
    }

    next()
}

module.exports = {
    error: error,

    buildError: buildError,

    DestError: buildError('Destination is wrong')
}
